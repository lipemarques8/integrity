<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\CodeFin\User::class, 1)
            ->states('admin')
            ->create([
                'name' => 'Felipe Marques',
                'email' => 'admin@user.com'
            ]);

        factory(\CodeFin\User::class, 1)
            ->create([
                'name' => 'Linaldo Lima',
                'email' => 'linaldo@user.com'
            ]);
    }
}
