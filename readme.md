# Itegrity

Primeiras Autenticações

* Estilização da área administrativa e alguns menus de exemplo;
* Autenticação da área administrativa;
* Autenticação da API com JWT;
* Criar uma rota /api/user, protegida pelo middleware guardião que mostre o nome do usuário autenticado.